# Teste Dev. React Native

O teste consiste em construir um aplicativo React Native de acordo com as imagens mostradas em cada seção, utilizando as API's abaixo.

###API's

###### http://staging.zaitt3.zaittgroup.com/api/v1/stores/1/init

Content-type: application/json

Key-words das categorias são usadas para filtrar a lista de produtos.

```json
{
    "total": 18,
    "id": 1,
    "name": "Zaitt Store Praia do Canto",
    "open": true,
    "token": "1d126ad605de37fadbc19669a8c42074",
    "address": "R. Joaquim Lírio, 89 - loja 1 - Praia do Canto, Vitória - ES, 29055-570",
    "picture_resized": "https://zaitt-3-production.s3.sa-east-1.amazonaws.com/RwpYGMPfSaiDmEiooYej25NG?response-content-disposition=inline%3B%20filename%3D%22Zaitt%201.png%22%3B%20filename%2A%3DUTF-8%27%27Zaitt%25201.png&response-content-type=image%2Fpng&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT5VERLH3KH2VJ6HT%2F20190118%2Fsa-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190118T144406Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=3bd635e1992106c85468618baba3354092c9de145f1eb382295482ee0c4372f9",
    "distance": null,
    "app": {
        "button_text_color": "#ffffff",
        "button_background_color": "#43e891",
        "logo": "https://zaitt-3-production.s3.sa-east-1.amazonaws.com/1zC4zRSNS5PheAZJrg8tbEBc?response-content-disposition=inline%3B%20filename%3D%22Zaittlogo%25402x.png%22%3B%20filename%2A%3DUTF-8%27%27Zaittlogo%25402x.png&response-content-type=image%2Fpng&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT5VERLH3KH2VJ6HT%2F20190118%2Fsa-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190118T144406Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=388440e7cf067626d5981c8818f58b47a78b3f1f0299112f220a92feaac98796",
        "header": "https://zaitt-3-production.s3.sa-east-1.amazonaws.com/NjCDgwqcKK3TSY5cy9oZSYpk?response-content-disposition=inline%3B%20filename%3D%229%20-%20Vitrine.png%22%3B%20filename%2A%3DUTF-8%27%279%2520-%2520Vitrine.png&response-content-type=image%2Fpng&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT5VERLH3KH2VJ6HT%2F20190118%2Fsa-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190118T144406Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=326a45c79818cac0d56a301e0558f686f3a40d0fde4f6e6b3f64a5bc0f2460c2"
    },
    "titles": [
        {
            "title": "",
            "categories": [
                {
                    "id": 33,
                    "category": "  ",
                    "key_words": "promoções",
                    "picture": "https://zaitt-3-production.s3.sa-east-1.amazonaws.com/mgBdbU6cMtE1rBn5ZMY8kKec?response-content-disposition=inline%3B%20filename%3D%22Promo%25402x.png%22%3B%20filename%2A%3DUTF-8%27%27Promo%25402x.png&response-content-type=image%2Fpng&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT5VERLH3KH2VJ6HT%2F20190118%2Fsa-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190118T145307Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=5457c31814ab67f2900eac3341ee8721d648632a99dc5aa1aa8be96407612c73"
                }
            ]
        },
        {
            "title": "Para comer",
            "categories": [
                {
                    "id": 24,
                    "category": "Bomboniere",
                    "key_words": "bomboniere",
                    "picture": "https://zaitt-3-production.s3.sa-east-1.amazonaws.com/TnAE9Pr2Sf3BHL7hFMv6teKc?response-content-disposition=inline%3B%20filename%3D%22MMs-by-the-Pound.jpg%22%3B%20filename%2A%3DUTF-8%27%27MMs-by-the-Pound.jpg&response-content-type=image%2Fjpeg&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT5VERLH3KH2VJ6HT%2F20190118%2Fsa-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190118T145307Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=2e9a97c744ca4dcea0daa1653b73393a68466cee434dace3f919330252b3c4a8"
                },
                {
                    "id": 26,
                    "category": "Churrasco",
                    "key_words": "churrasco",
                    "picture": "https://zaitt-3-production.s3.sa-east-1.amazonaws.com/19gBuisQgLNxKceJpkSkk1kX?response-content-disposition=inline%3B%20filename%3D%22churrasco.jpeg%22%3B%20filename%2A%3DUTF-8%27%27churrasco.jpeg&response-content-type=image%2Fjpeg&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT5VERLH3KH2VJ6HT%2F20190118%2Fsa-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190118T145307Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=d263a1182b8ed8e6594c396c55e0016bbdcc0cfdd87fff569c4345e5f450e4e4"
                },
                {
                    "id": 28,
                    "category": "Café da manhã",
                    "key_words": "matinal",
                    "picture": "https://zaitt-3-production.s3.sa-east-1.amazonaws.com/JiqadkTYhkhyZB4MXxuEXa2X?response-content-disposition=inline%3B%20filename%3D%22cafe-manha_20171205.jpg%22%3B%20filename%2A%3DUTF-8%27%27cafe-manha_20171205.jpg&response-content-type=image%2Fjpeg&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT5VERLH3KH2VJ6HT%2F20190118%2Fsa-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190118T145307Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=9f8c82b115a359b08d2c1b0d93722b3d076abb28befe042d52c611eba73a220e"
                },
                {
                    "id": 19,
                    "category": "Refeições",
                    "key_words": "refeição",
                    "picture": "https://zaitt-3-production.s3.sa-east-1.amazonaws.com/Q2zEgYzZmyXGJRoanV7RGrJk?response-content-disposition=inline%3B%20filename%3D%228%20-%20refeicoes.png%22%3B%20filename%2A%3DUTF-8%27%278%2520-%2520refeicoes.png&response-content-type=image%2Fpng&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT5VERLH3KH2VJ6HT%2F20190118%2Fsa-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190118T145307Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=7b7b08187c9d542df24666d66cafe3b506957cba97c324e8eb608fc39843bb3f"
                },
                {
                    "id": 10,
                    "category": "Biscoitos",
                    "key_words": "biscoito",
                    "picture": "https://zaitt-3-production.s3.sa-east-1.amazonaws.com/wamn69FU2YkAHyHR1PNBXFH8?response-content-disposition=inline%3B%20filename%3D%225%20-%20biscoitosepaes.png%22%3B%20filename%2A%3DUTF-8%27%275%2520-%2520biscoitosepaes.png&response-content-type=image%2Fpng&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT5VERLH3KH2VJ6HT%2F20190118%2Fsa-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190118T145307Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=a8f1102ba62c0d2c1e1c41d15bfb0994d5443a1f8d7999bc7f991e714aaa8fcf"
                },
                {
                    "id": 9,
                    "category": "Snacks",
                    "key_words": "snack",
                    "picture": "https://zaitt-3-production.s3.sa-east-1.amazonaws.com/mEGS5xMhkfRtkoxiQ22NPFeZ?response-content-disposition=inline%3B%20filename%3D%227%20-%20petiscos.png%22%3B%20filename%2A%3DUTF-8%27%277%2520-%2520petiscos.png&response-content-type=image%2Fpng&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT5VERLH3KH2VJ6HT%2F20190118%2Fsa-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190118T145307Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=97123dc1e88109605d5314aca268caa31ff57965c3714a4e8b76b6c386fc32ef"
                },
                {
                    "id": 11,
                    "category": "Chocolates",
                    "key_words": "chocolate",
                    "picture": "https://zaitt-3-production.s3.sa-east-1.amazonaws.com/hdnUgLdrQpKEppb5YUwhbQZe?response-content-disposition=inline%3B%20filename%3D%223%20-%20Chocolates.png%22%3B%20filename%2A%3DUTF-8%27%273%2520-%2520Chocolates.png&response-content-type=image%2Fpng&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT5VERLH3KH2VJ6HT%2F20190118%2Fsa-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190118T145307Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=07f46c22347a8c4b43fb84bc4c9c0a1d2d683e5c9cd4dc4a1454270b2d6ff632"
                },
                {
                    "id": 27,
                    "category": "Frios",
                    "key_words": "frios",
                    "picture": "https://zaitt-3-production.s3.sa-east-1.amazonaws.com/Bj9PBDGAjek1HajXWbhwbt41?response-content-disposition=inline%3B%20filename%3D%22Abbraccio-Frios2.jpg%22%3B%20filename%2A%3DUTF-8%27%27Abbraccio-Frios2.jpg&response-content-type=image%2Fjpeg&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT5VERLH3KH2VJ6HT%2F20190118%2Fsa-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190118T145307Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=576db75a038456e7a31776e001016eedc1842c5d430ae378ebb8c2116fdc7074"
                },
                {
                    "id": 32,
                    "category": "Sorvetes",
                    "key_words": "sorvete",
                    "picture": "https://zaitt-3-production.s3.sa-east-1.amazonaws.com/QGJ1MxSL8N7icn8xk4aVR1FL?response-content-disposition=inline%3B%20filename%3D%22capa-sorvetes.jpg%22%3B%20filename%2A%3DUTF-8%27%27capa-sorvetes.jpg&response-content-type=image%2Fjpeg&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT5VERLH3KH2VJ6HT%2F20190118%2Fsa-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190118T145307Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=25a65bc5449fb8c8cb87a0e231fb348b97677a0a023adb7fc513dc8118b38187"
                }
            ]
        },
        {
            "title": "Para beber",
            "categories": [
                {
                    "id": 17,
                    "category": "Bebidas",
                    "key_words": "bebidas",
                    "picture": "https://zaitt-3-production.s3.sa-east-1.amazonaws.com/fqjhcAeBYab2PQBaYgbCr38e?response-content-disposition=inline%3B%20filename%3D%221%20-%20Adega.png%22%3B%20filename%2A%3DUTF-8%27%271%2520-%2520Adega.png&response-content-type=image%2Fpng&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT5VERLH3KH2VJ6HT%2F20190118%2Fsa-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190118T145307Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=48450c2e0bb11fa7534056170e54479016078e318d272e5923478a708b5473e0"
                },
                {
                    "id": 20,
                    "category": "Vinhos",
                    "key_words": "vinho",
                    "picture": "https://zaitt-3-production.s3.sa-east-1.amazonaws.com/Cx8dDWXiMyeoF8PPAtgvMQCJ?response-content-disposition=inline%3B%20filename%3D%226%20-%20Destilados.png%22%3B%20filename%2A%3DUTF-8%27%276%2520-%2520Destilados.png&response-content-type=image%2Fpng&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT5VERLH3KH2VJ6HT%2F20190118%2Fsa-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190118T145307Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=44e581c19a2fa8ca4c4d4ec298ead53d37b6c1919700e62f4251500f509fbcc9"
                },
                {
                    "id": 25,
                    "category": "Soft drinks",
                    "key_words": "soft",
                    "picture": "https://zaitt-3-production.s3.sa-east-1.amazonaws.com/c9igK3D9zxaBgbgmnFkdhZCf?response-content-disposition=inline%3B%20filename%3D%22soft%20drinks.png%22%3B%20filename%2A%3DUTF-8%27%27soft%2520drinks.png&response-content-type=image%2Fpng&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT5VERLH3KH2VJ6HT%2F20190118%2Fsa-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190118T145307Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=f0a44fadb0fb59538da8f15ed900d2504b69e1217ce2c448ddc39ffb1558e637"
                },
                {
                    "id": 23,
                    "category": "Cafés",
                    "key_words": "café",
                    "picture": "https://zaitt-3-production.s3.sa-east-1.amazonaws.com/5NXFTtuHUNbfeY12MEdpWGDE?response-content-disposition=inline%3B%20filename%3D%22cafe-xicara2.jpg%22%3B%20filename%2A%3DUTF-8%27%27cafe-xicara2.jpg&response-content-type=image%2Fjpeg&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT5VERLH3KH2VJ6HT%2F20190118%2Fsa-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190118T145307Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=9f0a314fa7debb785933d1f13794f2a68caee02e4deb1ab9f3d10f6352ecaeb8"
                },
                {
                    "id": 31,
                    "category": "Destilados",
                    "key_words": "destilado",
                    "picture": "https://zaitt-3-production.s3.sa-east-1.amazonaws.com/zFHt1sd7bReNdcgBimTQqAp1?response-content-disposition=inline%3B%20filename%3D%22destilados2.jpg%22%3B%20filename%2A%3DUTF-8%27%27destilados2.jpg&response-content-type=image%2Fjpeg&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT5VERLH3KH2VJ6HT%2F20190118%2Fsa-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190118T145307Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=f79a67084a336af19a6c360423569dcf4ab65177c2d8296534860d59c8c653c9"
                },
                {
                    "id": 1,
                    "category": "Cervejas",
                    "key_words": "cerveja",
                    "picture": "https://zaitt-3-production.s3.sa-east-1.amazonaws.com/EWGZonoHVYaZixuDt2EKLWK1?response-content-disposition=inline%3B%20filename%3D%222%20-%20cervejas.png%22%3B%20filename%2A%3DUTF-8%27%272%2520-%2520cervejas.png&response-content-type=image%2Fpng&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT5VERLH3KH2VJ6HT%2F20190118%2Fsa-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190118T145307Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=8a970ba951f000c3c36566a41ddb13611a5ab9e56ad9b611849dd6977438bb95"
                }
            ]
        },
        {
            "title": "Acessórios",
            "categories": [
                {
                    "id": 30,
                    "category": "Tabacaria",
                    "key_words": "tabacaria",
                    "picture": "https://zaitt-3-production.s3.sa-east-1.amazonaws.com/pDBLE4jDqmC8cshWWiCTd4WL?response-content-disposition=inline%3B%20filename%3D%22tabacaria.jpg%22%3B%20filename%2A%3DUTF-8%27%27tabacaria.jpg&response-content-type=image%2Fjpeg&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT5VERLH3KH2VJ6HT%2F20190118%2Fsa-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190118T145307Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=bdacbd90a44fde771ce8c70b317918bc139c7427375ceef3644e9beed639ce7c"
                },
                {
                    "id": 29,
                    "category": "Farmacinha",
                    "key_words": "farmacia",
                    "picture": "https://zaitt-3-production.s3.sa-east-1.amazonaws.com/FosBvjcRvMkEQ54PevYfyxo9?response-content-disposition=inline%3B%20filename%3D%22farmacinha.jpg%22%3B%20filename%2A%3DUTF-8%27%27farmacinha.jpg&response-content-type=image%2Fjpeg&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT5VERLH3KH2VJ6HT%2F20190118%2Fsa-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190118T145307Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=354582171d258db0cbeddc1526f0590a6cf727e18bb9d801d1145a7ead50c5a3"
                }
            ]
        }
    ]
}
```

###### http://staging.zaitt3.zaittgroup.com/api/v1/stores/1/items?page=1&words=

Params: page, words.

Content-type: application/json

São exibidos 25 produtos em cada página.
Utilizar as key-words da categoria para filtrar os produtos.

```json
{
    "total": 540,
    "pages": 22,
    "items": [
        {
            "id": 464,
            "name": "3 Lobos Original",
            "brand": "Sem marca",
            "price": 1290,
            "key_words": "tabacaria",
            "product_key_words": "4",
            "description": null,
            "quantity": null,
            "store_id": 1,
            "picture_url": "https://zaitt-3-production.s3.sa-east-1.amazonaws.com/iDcRntwXd7RhEqqB6fYk3xhg?response-content-disposition=inline%3B%20filename%3D%22464.png%22%3B%20filename%2A%3DUTF-8%27%27464.png&response-content-type=image%2Fpng&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT5VERLH3KH2VJ6HT%2F20190118%2Fsa-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190118T145527Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=a2a9c9eb473b06c84f1e9ef82ab7f2d8794489697a1bd4f11aa92fdca3a29ef5",
            "adult": false
        },
        {
            "id": 2640,
            "name": "3 Lobos Palha Premium",
            "brand": "3 Lobos",
            "price": 1689,
            "key_words": "tabacaria, tabacaria, 3, lobos, palha, premium",
            "product_key_words": "4",
            "description": "",
            "quantity": "",
            "store_id": 1,
            "picture_url": "https://zaitt-3-production.s3.sa-east-1.amazonaws.com/btVDgFNr2CJiT3KnxfjinuqP?response-content-disposition=inline%3B%20filename%3D%22cigarro-premium%203%20lobos.jpg%22%3B%20filename%2A%3DUTF-8%27%27cigarro-premium%25203%2520lobos.jpg&response-content-type=image%2Fjpeg&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT5VERLH3KH2VJ6HT%2F20190118%2Fsa-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190118T145527Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=d364998478744c9f1db62b84013e998513ed58176e2938b08c81c2af0ef1562a",
            "adult": false
        },
        {
            "id": 1043,
            "name": "5star Lacta",
            "brand": "Sem marca",
            "price": 249,
            "key_words": "chocolate, doces, 5star, lacta",
            "product_key_words": "4",
            "description": null,
            "quantity": null,
            "store_id": 1,
            "picture_url": "https://s3-sa-east-1.amazonaws.com/zaitt-3-production/public/placeholder.png",
            "adult": false
        },
        {
            "id": 47,
            "name": "Absolut",
            "brand": "Sem marca",
            "price": 12990,
            "key_words": "destilado, bebidas, absolut, 1l",
            "product_key_words": "4",
            "description": "",
            "quantity": "1000 ml",
            "store_id": 1,
            "picture_url": "https://zaitt-3-production.s3.sa-east-1.amazonaws.com/8gNfiC3NM7ghGQujBEYYPspG?response-content-disposition=inline%3B%20filename%3D%2247.png%22%3B%20filename%2A%3DUTF-8%27%2747.png&response-content-type=image%2Fpng&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT5VERLH3KH2VJ6HT%2F20190118%2Fsa-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190118T145527Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=e4a0df7ddfc5840a69a682ef48df2992f9c9b56e92f58dd1b064fc65d714b339",
            "adult": false
        },
        {
            "id": 2240,
            "name": "Absorvente Interno OB",
            "brand": "OB",
            "price": 1099,
            "key_words": "farmacia, higiene pessoal, absorvente, interno, ob",
            "product_key_words": "4",
            "description": null,
            "quantity": null,
            "store_id": 1,
            "picture_url": "https://zaitt-3-production.s3.sa-east-1.amazonaws.com/kZtLaJ7YdVNRGJDcCLFNyC18?response-content-disposition=inline%3B%20filename%3D%222240.png%22%3B%20filename%2A%3DUTF-8%27%272240.png&response-content-type=image%2Fpng&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT5VERLH3KH2VJ6HT%2F20190118%2Fsa-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190118T145527Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=aa93226a5d718430bba07b8e462ee50b3f9bae46d2f459da0ac5e196d38226af",
            "adult": false
        },
        {
            "id": 502,
            "name": "Açai Frooty Banana com Granola",
            "brand": "Sem marca",
            "price": 790,
            "key_words": "sorvete, congelados, açai, frooty, banana, com, granola",
            "product_key_words": "4",
            "description": null,
            "quantity": null,
            "store_id": 1,
            "picture_url": "https://zaitt-3-production.s3.sa-east-1.amazonaws.com/b3JJtNAcHndQP94cxD3XULxy?response-content-disposition=inline%3B%20filename%3D%22502.png%22%3B%20filename%2A%3DUTF-8%27%27502.png&response-content-type=image%2Fpng&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT5VERLH3KH2VJ6HT%2F20190118%2Fsa-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190118T145527Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=7602661fac0b52de7abeb1b1d074df7af748256cc0c43ce78d7a94ca611e071c",
            "adult": false
        },
        {
            "id": 501,
            "name": "Açai Frooty com Granola",
            "brand": "Sem marca",
            "price": 990,
            "key_words": "sorvete, congelados, açai, frooty, com, granola",
            "product_key_words": "4",
            "description": null,
            "quantity": null,
            "store_id": 1,
            "picture_url": "https://zaitt-3-production.s3.sa-east-1.amazonaws.com/byrBNurmdVo6UD7d3VwSLJb8?response-content-disposition=inline%3B%20filename%3D%22501.png%22%3B%20filename%2A%3DUTF-8%27%27501.png&response-content-type=image%2Fpng&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT5VERLH3KH2VJ6HT%2F20190118%2Fsa-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190118T145527Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=2d990dee6f5bc534a470a58b809a547849c6d734126ce49edf92c87670aae014",
            "adult": false
        },
        {
            "id": 2746,
            "name": "Achocolatado Nescau",
            "brand": "Nescau",
            "price": 249,
            "key_words": ", achocolatado, nescau, 200, ml",
            "product_key_words": "4",
            "description": "",
            "quantity": "200 ml",
            "store_id": 1,
            "picture_url": "https://s3-sa-east-1.amazonaws.com/zaitt-3-production/public/placeholder.png",
            "adult": false
        },
        {
            "id": 2748,
            "name": "Achocolatado Nescau Zero",
            "brand": "Nescau",
            "price": 299,
            "key_words": "padaria, achocolatado, nescau, zero, 200, ml",
            "product_key_words": "4",
            "description": "",
            "quantity": "200 ml",
            "store_id": 1,
            "picture_url": "https://s3-sa-east-1.amazonaws.com/zaitt-3-production/public/placeholder.png",
            "adult": false
        },
        {
            "id": 2364,
            "name": "Achocolatado Ovomaltine",
            "brand": "Sem Marca",
            "price": 249,
            "key_words": "matinal, bebidas, achocolatado, ovomaltine, 180ml",
            "product_key_words": "4",
            "description": null,
            "quantity": "180 ml",
            "store_id": 1,
            "picture_url": "https://zaitt-3-production.s3.sa-east-1.amazonaws.com/dgs1Je9aC8J9FymsfRJ1RYNa?response-content-disposition=inline%3B%20filename%3D%222364.png%22%3B%20filename%2A%3DUTF-8%27%272364.png&response-content-type=image%2Fpng&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT5VERLH3KH2VJ6HT%2F20190118%2Fsa-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190118T145527Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=05c16c2688cf5ebf7482c85ca9679573aca1cbc9225de008624d76d39131b26a",
            "adult": false
        },
        {
            "id": 2402,
            "name": "Açucar União em Cubos",
            "brand": "União",
            "price": 799,
            "key_words": "matinal, mercearia, açucar, união, em, cubos, 250g",
            "product_key_words": "4",
            "description": null,
            "quantity": "250 g",
            "store_id": 1,
            "picture_url": "https://zaitt-3-production.s3.sa-east-1.amazonaws.com/mRcRwW6rzsNjSz3jEZ4YEpUr?response-content-disposition=inline%3B%20filename%3D%222402.png%22%3B%20filename%2A%3DUTF-8%27%272402.png&response-content-type=image%2Fpng&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT5VERLH3KH2VJ6HT%2F20190118%2Fsa-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190118T145527Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=d142140ed9b2a6687db788102c332f0c20e5fc8a6e29985f9a30dc27386d244e",
            "adult": false
        },
        {
            "id": 744,
            "name": "Adoçante Linea Stevia",
            "brand": "Sem marca",
            "price": 2890,
            "key_words": "matinal, mercearia, adoçante, linea, stevia",
            "product_key_words": "4",
            "description": null,
            "quantity": null,
            "store_id": 1,
            "picture_url": "https://zaitt-3-production.s3.sa-east-1.amazonaws.com/fXQQ1SFiLJygVmwNPqUAuPyf?response-content-disposition=inline%3B%20filename%3D%22744.png%22%3B%20filename%2A%3DUTF-8%27%27744.png&response-content-type=image%2Fjpeg&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT5VERLH3KH2VJ6HT%2F20190118%2Fsa-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190118T145527Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=226a5bc219fa5487c0f640cf94783603371c54395898abbb138b0565f0e1bbc0",
            "adult": false
        },
        {
            "id": 743,
            "name": "Adoçante Zero Cal",
            "brand": "Sem marca",
            "price": 1490,
            "key_words": "matinal, mercearia, adoçante, zero, cal",
            "product_key_words": "4",
            "description": null,
            "quantity": null,
            "store_id": 1,
            "picture_url": "https://zaitt-3-production.s3.sa-east-1.amazonaws.com/LeSgBDN7qziUFLk3GZ4GNMQb?response-content-disposition=inline%3B%20filename%3D%22743.png%22%3B%20filename%2A%3DUTF-8%27%27743.png&response-content-type=image%2Fjpeg&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT5VERLH3KH2VJ6HT%2F20190118%2Fsa-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190118T145527Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=1cd6832868e8afe7a8aed419e29c9bf0c131105afb474ab4963bba73fa61e716",
            "adult": false
        },
        {
            "id": 708,
            "name": "Água de Coco Triunfo",
            "brand": "Sem marca",
            "price": 399,
            "key_words": "soft, triunfo, água, de, côco, resfriada, 0,3l",
            "product_key_words": "4",
            "description": "",
            "quantity": "300 ml",
            "store_id": 1,
            "picture_url": "https://zaitt-3-production.s3.sa-east-1.amazonaws.com/snHvaFDsqo5MMHuBv9siitqe?response-content-disposition=inline%3B%20filename%3D%22708.png%22%3B%20filename%2A%3DUTF-8%27%27708.png&response-content-type=image%2Fpng&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT5VERLH3KH2VJ6HT%2F20190118%2Fsa-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190118T145527Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=1c61110cd837a1c11585ea26d35b2d10340389716268dddae0c7db049ba515c8",
            "adult": false
        },
        {
            "id": 2417,
            "name": "Água de Coco Triunfo",
            "brand": "Triunfo",
            "price": 599,
            "key_words": "bebidas",
            "product_key_words": "4",
            "description": "",
            "quantity": "500 ml",
            "store_id": 1,
            "picture_url": "https://zaitt-3-production.s3.sa-east-1.amazonaws.com/8Zc2E1X4WkoPSY2yCTx8QvHG?response-content-disposition=inline%3B%20filename%3D%22708.png%22%3B%20filename%2A%3DUTF-8%27%27708.png&response-content-type=image%2Fpng&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT5VERLH3KH2VJ6HT%2F20190118%2Fsa-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190118T145527Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=44f21996a8b7f0ca198276b95672318ef09bd8c0cb776574e1f0f961b541090c",
            "adult": false
        },
        {
            "id": 2416,
            "name": "Água de Coco Triunfo",
            "brand": "Triunfo",
            "price": 999,
            "key_words": "soft, bebidas, água, de, coco, triunfo, 1l, 1000ml",
            "product_key_words": "4",
            "description": "",
            "quantity": "1000 ml",
            "store_id": 1,
            "picture_url": "https://zaitt-3-production.s3.sa-east-1.amazonaws.com/aMngySF7NEMdgJbXC6MDZLGh?response-content-disposition=inline%3B%20filename%3D%22708.png%22%3B%20filename%2A%3DUTF-8%27%27708.png&response-content-type=image%2Fpng&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT5VERLH3KH2VJ6HT%2F20190118%2Fsa-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190118T145527Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=4e96f5b2e5e0682caf3e611d0b0f0e2ebd0daf0468a3cc83ea4f01e3c32bd70b",
            "adult": false
        },
        {
            "id": 84,
            "name": "Água Pedra Azul c/ gás",
            "brand": "Sem marca",
            "price": 299,
            "key_words": "soft, bebidas, água, pedra, azul, c/, gás, 500ml",
            "product_key_words": "4",
            "description": null,
            "quantity": "500 ml",
            "store_id": 1,
            "picture_url": "https://zaitt-3-production.s3.sa-east-1.amazonaws.com/QwCwcatqHKMxXu4LSyC1eTCF?response-content-disposition=inline%3B%20filename%3D%2284.png%22%3B%20filename%2A%3DUTF-8%27%2784.png&response-content-type=image%2Fpng&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT5VERLH3KH2VJ6HT%2F20190118%2Fsa-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190118T145527Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=6954b1f832bd142fcf9e1f0c33fea89a663b5a215108c40a957ee06360252826",
            "adult": false
        },
        {
            "id": 83,
            "name": "Água Pedra Azul s/ gás",
            "brand": "Sem marca",
            "price": 249,
            "key_words": "soft, bebidas, água, pedra, azul, s/, gás, 500ml",
            "product_key_words": "4",
            "description": null,
            "quantity": "500 ml",
            "store_id": 1,
            "picture_url": "https://zaitt-3-production.s3.sa-east-1.amazonaws.com/Xt4ooE8LHMazQk7KnGbKUKW9?response-content-disposition=inline%3B%20filename%3D%2283.png%22%3B%20filename%2A%3DUTF-8%27%2783.png&response-content-type=image%2Fpng&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT5VERLH3KH2VJ6HT%2F20190118%2Fsa-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190118T145527Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=85c7c5487e97a39c867db0d523fbed7b1576c62b4d9c87727dac9eea7dc80d27",
            "adult": false
        },
        {
            "id": 2549,
            "name": "Água Tônica Schin",
            "brand": "Schin",
            "price": 299,
            "key_words": "soft, bebidas, água, tônica, schin, 350, ml",
            "product_key_words": "4",
            "description": "",
            "quantity": "350 ml",
            "store_id": 1,
            "picture_url": "https://zaitt-3-production.s3.sa-east-1.amazonaws.com/M9sYqHZNY4KSnWTGcQXmt1Y9?response-content-disposition=inline%3B%20filename%3D%22schin%20tonica.jpg%22%3B%20filename%2A%3DUTF-8%27%27schin%2520tonica.jpg&response-content-type=image%2Fjpeg&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT5VERLH3KH2VJ6HT%2F20190118%2Fsa-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190118T145527Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=fbcca89c779f3db819ffea94b2072c8dd3986e39b0f11f66c4c2fabce6cc0046",
            "adult": false
        },
        {
            "id": 2267,
            "name": "Alpino Garrafa",
            "brand": "Alpino",
            "price": 490,
            "key_words": "matinal, bebidas, alpino, garrafa, 280ml",
            "product_key_words": "4",
            "description": null,
            "quantity": "280 ml",
            "store_id": 1,
            "picture_url": "https://zaitt-3-production.s3.sa-east-1.amazonaws.com/zY6FWuev9WouTPwjaSFEuUbQ?response-content-disposition=inline%3B%20filename%3D%222267.png%22%3B%20filename%2A%3DUTF-8%27%272267.png&response-content-type=image%2Fpng&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT5VERLH3KH2VJ6HT%2F20190118%2Fsa-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190118T145527Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=ed41ef167e847daefd59abaedcfd25f21a8b33e37ecf5df9e80a42cb5cb7d1c2",
            "adult": false
        },
        {
            "id": 765,
            "name": "Amandita Lacta",
            "brand": "Sem marca",
            "price": 999,
            "key_words": "bolacha, biscoitos, amandita, lacta, 200g",
            "product_key_words": "4",
            "description": null,
            "quantity": "200 g",
            "store_id": 1,
            "picture_url": "https://zaitt-3-production.s3.sa-east-1.amazonaws.com/2j8sKLdFFCue2GVwpxJpXtn7?response-content-disposition=inline%3B%20filename%3D%22765.png%22%3B%20filename%2A%3DUTF-8%27%27765.png&response-content-type=image%2Fjpeg&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT5VERLH3KH2VJ6HT%2F20190118%2Fsa-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190118T145527Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=b4ff1b716fafde9a707d2214656129ab56521f4273d241d85bc856df8748aa6e",
            "adult": false
        },
        {
            "id": 2720,
            "name": "Amendoim Descascado Salgado",
            "brand": "Yoki",
            "price": 449,
            "key_words": "snacks, snacks e salgadinhos, amendoim, descascado, salgado, 150, g",
            "product_key_words": "4",
            "description": "",
            "quantity": "150 g",
            "store_id": 1,
            "picture_url": "https://zaitt-3-production.s3.sa-east-1.amazonaws.com/ecTXrSieDon6BRkt4BfwdhuZ?response-content-disposition=inline%3B%20filename%3D%22Am-Desc-Salgado-150g-255x385.png%22%3B%20filename%2A%3DUTF-8%27%27Am-Desc-Salgado-150g-255x385.png&response-content-type=image%2Fpng&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT5VERLH3KH2VJ6HT%2F20190118%2Fsa-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190118T145527Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=d4a79cbe5bf444a57186bc1f5dc29978ff07ea1bfc1c87ee974ada490d490681",
            "adult": false
        },
        {
            "id": 2721,
            "name": "Amendoim Japonês Yoki",
            "brand": "Yoki",
            "price": 449,
            "key_words": "snacks, snacks e salgadinhos, amendoim, japonês, yoki, 150 g, japones",
            "product_key_words": "4",
            "description": "",
            "quantity": "150 g",
            "store_id": 1,
            "picture_url": "https://zaitt-3-production.s3.sa-east-1.amazonaws.com/mQSqVR2VFA4YBDWe1nXa4LyJ?response-content-disposition=inline%3B%20filename%3D%22Am-Japones-150g-256x385.png%22%3B%20filename%2A%3DUTF-8%27%27Am-Japones-150g-256x385.png&response-content-type=image%2Fpng&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT5VERLH3KH2VJ6HT%2F20190118%2Fsa-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190118T145527Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=57469496303c8f5804e851ce32fba37c4a6cf8306637e8e4d4240436914a081e",
            "adult": false
        },
        {
            "id": 991,
            "name": "Amor à Fruta - Bete Balança",
            "brand": "Sem marca",
            "price": 1050,
            "key_words": "soft, triunfo, amor, à, fruta, -, bete, balança",
            "product_key_words": "4",
            "description": "",
            "quantity": "",
            "store_id": 1,
            "picture_url": "https://s3-sa-east-1.amazonaws.com/zaitt-3-production/public/placeholder.png",
            "adult": false
        },
        {
            "id": 990,
            "name": "Amor à Fruta - Verdelícia",
            "brand": "Sem marca",
            "price": 1050,
            "key_words": "soft, triunfo, amor, à, fruta, -, verdelícia",
            "product_key_words": "4",
            "description": "",
            "quantity": "",
            "store_id": 1,
            "picture_url": "https://s3-sa-east-1.amazonaws.com/zaitt-3-production/public/placeholder.png",
            "adult": false
        }
    ]
}
```

### Carrinho

O aplicativo necessíta ter um carrinho para o armazenamento de produtos.

1. Deverá ser feito obrigatoriamente em Redux.
2. Deverá exibir a quantidade total e preço total dos produtos adicionados.
3. O carrinho deve ser exibido a todo momento quando houver pelo menos 1 produto e desaparecer quando estiver vazio.
4. O carrinho deve ser exibido em todas as telas do app.
5. O design do carrinho fica a critério do desenvolvedor.

### Tela da Vitrine

[Link da imagem](https://bitbucket.org/new-zaitt/react-native-test/raw/master/imgs/Vitrine.png)

1. Será necessário exibir os dados recebidos de acordo com a imagem.
2. A header precisa ser exibida com a url do endpoint.
3. A logo necessita ser exibida junto com a header.
4. Exibir o nome da loja na header.
5. As categorias deverão ser exibidas de forma semelhante a imagem.

### Tela de Produtos da Categoria

[Link da imagem](https://bitbucket.org/new-zaitt/react-native-test/raw/master/imgs/ProdutosCategoria.png)

1. Necessário exibir os produtos de acordo com a imagem.
2. A lista utilizará paginação. Ao chegar no fim da lista, será necessário realizar uma requisição no endpoint e adicionar esses produtos na lista, até chegar no seu total.
3. Os preços deverão ser convertidos para a exibição no app. Ex: 399 = 3,99.
4. Todos os produtos da lista obrigatoriamente necessitam ter a opção para adicionar/remover do carrinho.
5. Ao clicar no produto, você deverá exibir um modal ou uma tela com os detalhes do mesmo.
6. Ao clicar no botão voltar, deverá ser limpa a lista dos produtos e voltar para vitrine.

### Exibição do produto

[Link da imagem](https://bitbucket.org/new-zaitt/react-native-test/raw/master/imgs/ModalProduto.png)

1. Você deve exibir os dados do produto de acordo com a imagem.
2. Poderá ser utilizado um modal ou uma tela para sua exibição.
3. Se for uma tela, será necessário um botão para voltar a tela anterior.
4. O modal/tela deve ter a funcionalidade de adicionar o produto no carrinho.
5. Só sera adicionado no carrinho quando clicar em adicionar, e deve ser toda a quantidade indicada de uma só vez.
6. Não é necessário exibir a descrição do produto.

### Bonus

Realizar tarefas dessa seção te dará mais pontos no teste.

1. Armazenar os dados da Vitrine em Redux.
2. Armazenar a lista de produtos da categoria em Redux.
3. Exibir a quantidade do produto no carrinho em seu botão, na tela de produtos da categoria.
4. Criar um componente reutilizavel no aplicativo.

### Observações

1. Você deverá enviar um e-mail avisando sobre a finalização do teste, para contato@zaitt.com.br.    
2. O projeto poderá ser desenvolvido via Expo ou a CLI do React Native (react-native init).    
3. Você poderá utilizar bibliotecas para adicionar funcionalidades no projeto.
4. Será avaliado principalmente as funcionalidades desenvolvidas para o aplicativo. Não é obrigatório seguir o layout à risca, mas contará pontos caso fiquei bem próximo as imagens.
5. Você deve versionar o projeto no bitbucket (privado) e adicionar como membro do projeto: luiz.normanha@zaitt.com.br.
6. Caso o projeto seja feito via Expo, deverá ser enviando o link do mesmo via e-mail.